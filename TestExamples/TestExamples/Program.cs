﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestExamples
{

    class A
    {
        public virtual void DoWork() { Console.WriteLine("1"); }
        public void DoWork2() { Console.WriteLine("2"); }
    }
    class B : A
    {
        public override void DoWork() { Console.WriteLine("3"); }
        public new void DoWork2() { Console.WriteLine("4"); }
    }
    class C : B
    {
        public override void DoWork() { Console.WriteLine("5"); }
        public new void DoWork2() { Console.WriteLine("6"); }
    }

    class Program
    {
        static void Main(string[] args)
        {
            A aa = new A();
            A ab = new B();
            A ac = new C();
            B bb = new B();
            B bc = new C();
            C cc = new C();

            aa.DoWork();//метод DoWork класса А---
            aa.DoWork2();//метод DoWork2 класса А---
            
            ab.DoWork();//метод DoWork класса B:A---
            ab.DoWork2();//метод DoWork2 класса В---

            ac.DoWork();//метод DoWork класса C:B---
            ac.DoWork2();//метод DoWork2 класса C---

            bb.DoWork(); //метод DoWork класса B:A---
            bb.DoWork2();//метод DoWork2 класса B---

            bc.DoWork();//метод DoWork класса C:B ---
            bc.DoWork2();//метод DoWork2 класса C---

            cc.DoWork();//метод DoWork класса C:B---
            cc.DoWork2();//метод DoWork2 класса C---

            Console.ReadLine();
        }
    }
}
